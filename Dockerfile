FROM openjdk:8

RUN apt-get update && apt-get install -y iputils-ping telnet graphviz

COPY mssql-jdbc-9.4.0.jre8.jar /opt/schemaspy/mssql-jdbc-9.4.0.jre8.jar
COPY schemaspy-6.2.4.jar /opt/schemaspy/schemaspy-6.2.4.jar
COPY schemaspy.properties /opt/schemaspy/schemaspy.properties

WORKDIR /opt/schemaspy

CMD ["java", "-Djavax.net.ssl.trustStoreType=Windows-ROOT", "-jar", "schemaspy-6.2.4.jar", "-connprops encrypt\\=false"]