# About the project

This project you can customize and build your SchemaSpy image, according to you database configuration.

## How to use it?

All the required packages are already instaled and configured to use MSSQL SERVER. You just need to update the schemaspy.properties with you database address and credentials. Then you are ready to build and run the image. Example: docker run -it -v /the/path/to/output:/opt/schemaspy/output carlosdiegodev/schemaspy:mssql